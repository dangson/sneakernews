<?php

namespace SneakerNews;

use Illuminate\Database\Eloquent\Model;
use SneakerNews\Helpers\StringHelper;
use SneakerNews\Scopes\LimitByForeignKey;

class Post extends Model
{
    use LimitByForeignKey;

    protected $fillable = [
        'pico_id',
        'title',
        'url',
        'publish_date',
        'author',
        'content',
        'feed_id',
    ];

    /**
     * Returns content stripped of HTML and truncated
     * @return  string
     */
    public function getContentShortenedAttribute()
    {
        $content = strip_tags($this->attributes['content']);
        return StringHelper::truncateAtWord($content, 255);
    }
}
