<?php

namespace SneakerNews;

use Illuminate\Database\Eloquent\Model;

use SneakerNews\Feed;

class Category extends Model
{
    protected $fillable = [
        'name',
        'display_order',
    ];

    public function feeds() {
    	return $this->hasMany(Feed::class);
    }
}
