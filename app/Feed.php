<?php

namespace SneakerNews;

use Illuminate\Database\Eloquent\Model;

use SneakerNews\Post;

class Feed extends Model
{
    protected $fillable = [
        'name',
        'url',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

	public function posts()
    {
    	return $this->hasMany(Post::class)->latest('publish_date');
    }

    public function latestPosts()
    {
        return $this->hasMany(Post::class)->latest('publish_date')->limitByForeignKey('feed_id', '10');
    }
}
