<?php

namespace SneakerNews\Console\Commands;

use Cache;
use Illuminate\Console\Command;
use SneakerNews\Helpers\FeedParser;

class ParseFeeds extends Command
{
    protected $feedParser;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feeds:parse {feedId? : Optional ID to pull a single feed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse RSS feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(FeedParser $feedParser)
    {
        parent::__construct();
        $this->feedParser = $feedParser;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feedId = $this->argument('feedId');

        $feedsProcessedSucessfully = $this->feedParser->parse($feedId);

        if ($feedsProcessedSucessfully > 0) {
            Cache::forget('categoriesWithFeedsAndPosts');
            $this->info('Finished parsing feeds!');
        }
        else {
            $this->error('No feeds were processed. Check the log.');
        }
    }
}
