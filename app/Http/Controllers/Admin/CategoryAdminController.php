<?php

namespace SneakerNews\Http\Controllers\Admin;

use SneakerNews\Http\Controllers\Controller;
use SneakerNews\Http\Requests\StoreOrUpdateCategoryRequest;
use SneakerNews\Repositories\Eloquent\CategoryRepository;

class CategoryAdminController extends Controller
{
    protected $categoryRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->middleware('auth');
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryRepository->with('feeds')->orderBy('display_order')->all();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = $this->categoryRepository->model;
        return view('admin.categories.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreOrUpdateCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateCategoryRequest $request)
    {
        $this->categoryRepository->store($request->all());
        return redirect()->route('admin.categories.index')->with('message', 'New category has been created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->with('feeds')->find($id);
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreOrUpdateCategoryRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreOrUpdateCategoryRequest $request, $id)
    {
        $this->categoryRepository->update($request->all(), $id);
        return redirect()->route('admin.categories.index')->with('message', 'Changes have been saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->categoryRepository->delete($id);
        return redirect()->route('admin.categories.index')->with('message', 'Category has been deleted.');
    }
}
