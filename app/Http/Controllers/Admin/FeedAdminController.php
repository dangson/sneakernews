<?php

namespace SneakerNews\Http\Controllers\Admin;

use Artisan;
use SneakerNews\Http\Controllers\Controller;
use SneakerNews\Http\Requests\StoreOrUpdateFeedRequest;
use SneakerNews\Repositories\Eloquent\FeedRepository;
use SneakerNews\Repositories\Eloquent\CategoryRepository;

class FeedAdminController extends Controller
{
    protected $feedRepository;
    protected $categoryRepository;

    public function __construct(FeedRepository $feedRepository, CategoryRepository $categoryRepository)
    {
        $this->middleware('auth');

        $this->feedRepository = $feedRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeds = $this->feedRepository->with('category')->orderBy('name')->all();
        return view('admin.feeds.index', compact('feeds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $feed = $this->feedRepository->model;
        $categories = $this->categoryRepository->orderBy('name')->lists('name', 'id');

        return view('admin.feeds.create', compact('feed', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreOrUpdateFeedRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateFeedRequest $request)
    {
        $feed = $this->feedRepository->store($request->all());

        Artisan::queue('feeds:parse', [
            'feedId' => $feed->id,
        ]);

        return redirect()->route('admin.feeds.index')->with('message', 'New feed has been created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feed = $this->feedRepository->with('category')->find($id);
        $categories = $this->categoryRepository->orderBy('name')->lists('name', 'id');

        return view('admin.feeds.edit', compact('feed', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreOrUpdateFeedRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreOrUpdateFeedRequest $request, $id)
    {
        $this->feedRepository->update($request->all(), $id);
        return redirect()->route('admin.feeds.index')->with('message', 'Changes have been saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->feedRepository->delete($id);
        return redirect()->route('admin.feeds.index')->with('message', 'Feed has been deleted.');
    }
}
