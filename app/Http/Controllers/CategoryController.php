<?php

namespace SneakerNews\Http\Controllers;

use Cache;
use Illuminate\Http\Request;

use SneakerNews\Http\Requests;
use SneakerNews\Http\Controllers\Controller;
use SneakerNews\Repositories\Eloquent\CategoryRepository;

class CategoryController extends Controller
{
    protected $categoryRepository;

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

	/**
	 * Display categories along with associated feeds and posts
	 * @param   Request  $request
	 * @return  Response
	 */
    public function index(Request $request) {
        $categories = Cache::remember('categoriesWithFeedsAndPosts', 60, function() {
            return $this->categoryRepository->with('feeds.latestPosts')->orderBy('display_order')->all();
        });

    	return view('categories.index', compact('categories'));
    }
}
