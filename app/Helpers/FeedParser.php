<?php namespace SneakerNews\Helpers;

use Log;
use SneakerNews\Repositories\Eloquent\FeedRepository;
use SneakerNews\Repositories\Eloquent\PostRepository;
use PicoFeed\Reader\Reader;

class FeedParser
{
  protected $feedRepository;
  protected $postRepository;
  protected $picoReader;

  public function __construct(FeedRepository $feedRepository, PostRepository $postRepository, Reader $picoReader)
  {
    $this->feedRepository = $feedRepository;
    $this->postRepository = $postRepository;
    $this->picoReader = $picoReader;
  }

  /**
   * Handle parsing of feeds
   * @param   int  $feedId
   * @return  int  $feedsProcessedSucessfully
   */
  public function parse($feedId = null)
  {
    if (!empty($feedId)) {
      $feeds = $this->feedRepository->find($feedId)->get();
    }
    else {
      $feeds = $this->feedRepository->all();
    }

    $postUniqueIds = $this->postRepository->lists('pico_id')->toArray();

    $feedsProcessedSucessfully = 0;

    foreach ($feeds AS $feed) {
      try {
        $this->download($feed, $postUniqueIds);
        $feedsProcessedSucessfully++;
      }
      catch (Exception $e) {
        Log::error("Unable to parse feed $feed->name - " . $e->getMessage());
      }
    }

    return $feedsProcessedSucessfully;
  }

  /**
   * Download and save posts
   * @param   Feed  $feed           Feed to download
   * @param   Array $postUniqueIds  Unique IDs of existing posts
   * @return  void
   */
  private function download($feed, $postUniqueIds)
  {
    try {
      $resource = $this->picoReader->download($feed->url, $feed->last_modified, $feed->etag);

      if ($resource->isModified()) {
        $parser = $this->picoReader->getParser(
          $resource->getUrl(),
          $resource->getContent(),
          $resource->getEncoding()
        );

        $response = $parser->execute();

        $feed->etag = $resource->getEtag();
        $feed->last_modified = $resource->getLastModified();
        $feed->save();

        foreach ($response->items AS $item) {
          if (!in_array($item->getId(), $postUniqueIds)) {
              $this->savePost($item, $feed->id);
          }
        }
      }
      else {
        Log::info("Feed $feed->name not modified");
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Save new Post using data from parser
   * @param   Item  $feedItem  Feed Item from picoFeed
   * @param   int   $feedId    ID of feed this post is associated with
   * @return  Post             Newly created Post object
   */
  private function savePost($feedItem, $feedId)
  {
    $post = [
      'pico_id'      => $feedItem->getId(),
      'title'        => $feedItem->getTitle(),
      'url'          => $feedItem->getUrl(),
      'publish_date' => $feedItem->getDate(),
      'author'       => $feedItem->getAuthor(),
      'content'      => $feedItem->getContent(),
      'feed_id'      => $feedId,
    ];

    return $this->postRepository->store($post);
  }
}