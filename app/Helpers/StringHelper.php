<?php namespace SneakerNews\Helpers;

class StringHelper
{
  /**
   * Truncate a string to the nearest word
   * @param   string  $text
   * @param   int     $length
   * @param   bool    $includeEllipsis
   * @return  string
   */
  public static function truncateAtWord($text, $length, $includeEllipsis = true)
  {
    if (strlen($text) < $length) {
      return $text;
    }

		if (preg_match("/^.{1,$length}\b/s", $text, $match)) {
      $ellipsis = $includeEllipsis ? '&hellip;' : '';
			return trim($match[0]) . $ellipsis;
		}

    return $text;
  }
}