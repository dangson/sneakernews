<?php namespace SneakerNews\Scopes;

trait LimitByForeignKey
{
	/**
	 * Query scope to limit results by foreign key
	 * @return void
	 */
	public function scopeLimitByForeignKey($query, $foreignKey, $limit = 10)
	{
		// Raw queries don't insert table prefix automatically
		$tableWithPrefix = env('DB_PREFIX') . $this->getTable();

		// Build select clause with grouping and ranking variables
		$groupAlias = 'group_id_for_limiter';
		$rankAlias  = 'rank_for_limiter';

		$query->selectRaw(
			"{$tableWithPrefix}.*, @rank := IF(@group = {$foreignKey}, @rank + 1, 1) as {$rankAlias}, @group := {$foreignKey} as {$groupAlias}"
		);

		// Set default variables in from clause
		$query->from(\DB::raw("(SELECT @rank := 0, @group := 0) as default_variables, {$tableWithPrefix}"));

		// Order by the foreign key, before other order clauses
		$orderClauses = $query->getQuery()->orders;
		$foreignKeyOrder = ['column' => $foreignKey, 'direction' => 'asc'];

		if (!empty($orderClauses)) {
			array_unshift($orderClauses, $foreignKeyOrder);
		}
		else {
			$orderClauses[] = $foreignKeyOrder;
		}

		$query->getQuery()->orders = $orderClauses;

		$subQuery = $query->toSql();

		// Build new query that uses the above subquery
		$newQuery = $this->newQuery()
			->from(\DB::raw("({$subQuery}) as {$tableWithPrefix}"))
			->mergeBindings($query->getQuery())
			->where($rankAlias, '<=', $limit)
			->getQuery();

		$query->setQuery($newQuery);
	}
}