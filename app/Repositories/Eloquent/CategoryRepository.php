<?php namespace SneakerNews\Repositories\Eloquent;

use SneakerNews\Repositories\Eloquent\RepositoryAbstract;

class CategoryRepository extends RepositoryAbstract
{
  public function model()
  {
    return 'SneakerNews\Category';
  }
}