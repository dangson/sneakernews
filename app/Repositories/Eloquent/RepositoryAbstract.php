<?php namespace SneakerNews\Repositories\Eloquent;

use SneakerNews\Repositories\Contracts\RepositoryInterface;
use SneakerNews\Exceptions\RepositoryException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

abstract class RepositoryAbstract implements RepositoryInterface
{
  private $app;
  public $model;
  protected $with = [];
  protected $orders = [];

  public function __construct(App $app)
  {
    $this->app = $app;
    $this->makeModel();
  }

  /**
   * Return model class name
   * @return  string
   */
  abstract function model();

  /**
   * Return all records
   * @param   array   $columns
   * @return  mixed
   */
  public function all($columns = array('*'))
  {
    return $this->newQuery()->get($columns);
  }

  /**
   * Return all records with paginator
   * @param   integer  $perPage
   * @param   array    $columns
   * @return  mixed
   */
  public function paginate($perPage = 15, $columns = array('*'))
  {
    return $this->newQuery()->paginate($perPage, $columns);
  }

  /**
   * Store a new record to the database
   * @param   array   $data
   * @return  mixed
   */
  public function store(array $data)
  {
    return $this->model->create($data);
  }

  /**
   * Update an existing record
   * @param   array   $data
   * @param   int     $id
   * @return  bool
   */
  public function update(array $data, $id)
  {
    $model = $this->model->findOrFail($id);
    $model->fill($data);

    return $model->save();
  }

  /**
   * Delete a record
   * @param   int  $id
   * @return  mixed
   */
  public function delete($id)
  {
    return $this->model->destroy($id);
  }

  /**
   * Find a specific record by ID
   * @param   int    $id
   * @param   array  $columns
   * @return  mixed
   */
  public function find($id, $columns = array('*'))
  {
    return $this->newQuery()->find($id, $columns);
  }

  /**
   * Find a specific record by attribute
   * @param   string  $attribute
   * @param   string  $value
   * @param   array   $columns
   * @return  mixed
   */
  public function findBy($attribute, $value, $columns = array('*'))
  {
    return $this->newQuery()->where($attribute, $value)->first($columns);
  }

  /**
   * Create an instance of the Eloquent model
   * @return  \Illuminate\Database\Eloquent\Builder
   * @throws  RepositoryException
   */
  public function makeModel()
  {
    $model = $this->app->make($this->model());

    if (!$model instanceof Model) {
      throw new RepositoryException("Class " . $this->model() . " must must be an instance of Illuminate\\Database\\Eloquent\\Model");
    }

    return $this->model = $model;
  }

  /**
   * Assign relations for eager loading
   * @param   string  $relations
   * @return  $this
   */
  public function with($relations)
  {
    if (is_string($relations)) {
      $relations = func_get_args();
    }

    $this->with = $relations;

    return $this;
  }

  /**
   * Set order by clauses for query
   * @param   string  $column
   * @param   string  $direction
   * @return  $this
   */
  public function orderBy($column, $direction = 'asc')
  {
    $this->orders[] = compact('column', 'direction');
    return $this;
  }

  /**
   * Returns array of values for the given column
   * @param   string  $column
   * @param   string  $key
   * @return  array
   */
  public function lists($column, $key = null)
  {
    return $this->model->lists($column, $key);
  }

  /**
   * Create new query taking into account order and eager loading
   * @return  Builder
   */
  protected function newQuery()
  {
    $query = $this->model->newQuery();

    if (!empty($this->orders)) {
      foreach ($this->orders AS $order) {
        $query->orderBy($order['column'], $order['direction']);
      }
    }

    return $query->with($this->with);
  }
}