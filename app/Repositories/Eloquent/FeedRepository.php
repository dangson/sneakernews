<?php namespace SneakerNews\Repositories\Eloquent;

use SneakerNews\Repositories\Eloquent\RepositoryAbstract;

class FeedRepository extends RepositoryAbstract
{
  public function model()
  {
    return 'SneakerNews\Feed';
  }
}