<?php namespace SneakerNews\Repositories\Eloquent;

use SneakerNews\Repositories\Eloquent\RepositoryAbstract;

class PostRepository extends RepositoryAbstract
{
  public function model()
  {
    return 'SneakerNews\Post';
  }
}