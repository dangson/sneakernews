<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;

class FeedAdminControllerTest extends TestCase
{
  use WithoutMiddleware;

  public function testIndex()
  {
    $feed = factory(SneakerNews\Feed::class)->create();

    $this->visit('/admin/feeds')
         ->see('Test Feed');
  }

  public function testStore()
  {
    // Create a category so there is at least one available for selection
    $category = factory(SneakerNews\Category::class)->create();
    $feed = factory(SneakerNews\Feed::class)->make();

    $this->visit('/admin/feeds/create')
         ->type($feed->name, 'name')
         ->type($feed->url, 'url')
         ->press('Save Feed')
         ->seeInDatabase('feeds', ['name' => $feed->name]);
  }

  public function testStoreValidationFails()
  {
    $this->visit('/admin/feeds/create')
         ->press('Save Feed')
         ->assertSessionHasErrors();
  }

  public function testUpdate()
  {
    $feed = factory(SneakerNews\Feed::class)->create();

    $this->visit('/admin/feeds/' . $feed->id . '/edit')
         ->type('Updated Feed Name', 'name')
         ->press('Update Feed')
         ->seeInDatabase('feeds', ['name' => 'Updated Feed Name']);
  }

  public function testDestroy()
  {
    $feed = factory(SneakerNews\Feed::class)->create();

    $this->visit('/admin/feeds/' . $feed->id . '/edit')
         ->press('Delete')
         ->see('Feed has been deleted.');
  }
}
?>