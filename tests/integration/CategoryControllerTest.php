<?php

class CategoryControllerTest extends TestCase
{
  public function testIndex()
  {
    $category = factory(SneakerNews\Category::class)->create();

    $feed = factory(SneakerNews\Feed::class)->create([
      'category_id' => $category->id,
    ]);

    $post = factory(SneakerNews\Post::class)->create([
      'feed_id' => $feed->id,
    ]);

    $this->visit('/')
         ->see($category->name)
         ->see($feed->name);
  }
}
?>