<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;

class CategoryAdminControllerTest extends TestCase
{
  use WithoutMiddleware;

  public function testIndex()
  {
    $category = factory(SneakerNews\Category::class)->create();

    $this->visit('/admin/categories')
         ->see($category->name);
  }

  public function testStore()
  {
    $this->visit('/admin/categories/create')
         ->type('New Test Category', 'name')
         ->type(9, 'display_order')
         ->press('Save Category')
         ->seeInDatabase('categories', ['name' => 'New Test Category']);
  }

  public function testStoreValidationFails()
  {
    $this->visit('/admin/categories/create')
         ->press('Save Category')
         ->assertSessionHasErrors();
  }

  public function testUpdate()
  {
    $category = factory(SneakerNews\Category::class)->create();

    $this->visit('/admin/categories/' . $category->id . '/edit')
         ->type('Updated Category Name', 'name')
         ->press('Update Category')
         ->seeInDatabase('categories', ['name' => 'Updated Category Name']);
  }

  public function testDestroy()
  {
    $category = factory(SneakerNews\Category::class)->create();

    $this->visit('/admin/categories/' . $category->id . '/edit')
         ->press('Delete')
         ->see('Category has been deleted.');
  }
}
?>