<?php
class FeedParserTest extends TestCase
{
  protected $feedParser;

  public function setUp()
  {
    parent::setUp();
    $this->feedParser = $this->app->make(SneakerNews\Helpers\FeedParser::class);
  }

  public function testParseSingleFeed()
  {
    $feed = factory(SneakerNews\Feed::class)->create();
    $feedsProcessedSucessfully = $this->feedParser->parse($feed->id);

    $this->assertGreaterThan(0, $feedsProcessedSucessfully);
  }
}
?>