<?php

use SneakerNews\Helpers\StringHelper;

class StringHelperTest extends TestCase
{
  public function testTruncateAtWord()
  {
    $text = 'The fox jumped over the lazy dog.';
    $truncatedText = StringHelper::truncateAtWord($text, 10, false);

    $this->assertEquals('The fox', $truncatedText);
  }

  public function testTruncateAtWordWithEllipsis()
  {
    $text = 'The fox jumped over the lazy dog.';
    $truncatedText = StringHelper::truncateAtWord($text, 10);

    $this->assertEquals('The fox&hellip;', $truncatedText);
  }

  public function testTruncateAtWordNoTruncation()
  {
    $text = 'The fox jumped over the lazy dog.';
    $truncatedText = StringHelper::truncateAtWord($text, 100, false);

    $this->assertEquals($text, $truncatedText);
  }
}