$(document).ready(function() {
    /* Enable Boostrap popovers */
    $('.post-link').popover({
        'placement': 'top',
        'trigger': 'hover',
        'delay': {
            'show': 500,
            'hide': 100
        }
    });

    /* Confirm when user tries to delete something */
    $('.delete-form').on('submit', function() {
        return confirm('Are you sure you want to delete this item? This cannot be undone.');
    });
});