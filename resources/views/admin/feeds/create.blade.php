@extends('layouts.app')

@section('content')

<div class='container'>
  <div class='row'>
    <div class='col-md-12'>
      <h3>New Feed</h3>

      @include('common._form-errors')

      {!! Form::open(['route' => 'admin.feeds.store', 'class' => 'form-horizontal']) !!}

      @include('admin.feeds._fields', ['submitLabel' => 'Save Feed'])

      {!! Form::close() !!}
    </div>
  </div>
</div>

@endsection