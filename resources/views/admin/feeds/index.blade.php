@extends('layouts.app')

@section('content')

<div class='container'>
  <div class='row'>
    <div class='col-md-12'>
      <h3>Feeds</h3>

      @include('common._flashed-message')

      <p>
        <a href="{{ route('admin.feeds.create') }}" class='btn btn-default'>Add Feed</a>
      </p>

      <table class='table table-striped table-bordered'>
        <thead>
          <tr>
            <th>Feed</th>
            <th>URL</th>
            <th width='20%'>Category</th>
            <th width='15%'>Edit</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($feeds AS $feed)
            <tr>
              <td>{{ $feed->name }}</td>
              <td><a href="{{ $feed->url }}" target='_blank'><small>{{ $feed->url }}</small></a></td>
              <td>{{ $feed->category->name }}</td>
              <td><a href="{{ route('admin.feeds.edit', $feed->id) }}">Edit</a></td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection