<div class='form-group'>
  <label for='name' class='col-md-2 control-label'><span class='required'>*</span> Name:</label>
  <div class='col-md-4'>
    {!! Form::text('name', $feed->name, ['class' => 'form-control']) !!}
  </div>
</div>

<div class='form-group'>
  <label for='url' class='col-md-2 control-label'><span class='required'>*</span> URL:</label>
  <div class='col-md-4'>
    {!! Form::text('url', $feed->url, ['class' => 'form-control']) !!}
  </div>
</div>

<div class='form-group'>
  <label for='category_id' class='col-md-2 control-label'><span class='required'>*</span> Category:</label>
  <div class='col-md-4'>
    {!! Form::select('category_id', $categories, $feed->category_id, ['class' => 'form-control']) !!}
  </div>
</div>

<div class='form-group'>
  <div class='col-md-10 col-md-offset-2'>
    {!! Form::submit($submitLabel, ['class' => 'btn btn-default']) !!}
  </div>
</div>