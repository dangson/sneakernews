@extends('layouts.app')

@section('content')

<div class='container'>
  <div class='row'>
    <div class='col-md-12'>
      <h3>Feed: {{ $feed->name }}</h3>

      @include('common._form-errors')

      {!! Form::open(['method' => 'PUT', 'route' => ['admin.feeds.update', $feed->id], 'class' => 'form-horizontal']) !!}

      @include('admin.feeds._fields', ['submitLabel' => 'Update Feed'])

      {!! Form::close() !!}

      <div class='form-group'>
        <div class='col-md-12 text-right'> {!! Form::open(['method' => 'DELETE', 'route' => ['admin.feeds.destroy', $feed->id], 'class' => 'delete-form']) !!}
          {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection