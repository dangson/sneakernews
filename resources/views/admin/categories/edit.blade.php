@extends('layouts.app')

@section('content')

<div class='container'>
  <div class='row'>
    <div class='col-md-12'>
      <h3>Category: {{ $category->name }}</h3>

      @include('common._form-errors')

      {!! Form::open(['method' => 'PUT', 'route' => ['admin.categories.update', $category->id], 'class' => 'form-horizontal']) !!}

      @include('admin.categories._fields', ['submitLabel' => 'Update Category'])

      {!! Form::close() !!}

      <div class='form-group'>
        <div class='col-md-12 text-right'>
          {!! Form::open(['method' => 'DELETE', 'route' => ['admin.categories.destroy', $category->id], 'class' => 'delete-form']) !!}
          {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
          {!! Form::close() !!}
        </div>
      </div>

      <h4>Category Feeds</h4>

      <table class='table table-striped table-bordered'>
        <thead>
          <tr>
            <th>Feed</th>
            <th width='15%'>Edit</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($category->feeds AS $feed)
            <tr>
              <td>{{ $feed->name }}</td>
              <td><a href="#">Edit</a></td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection