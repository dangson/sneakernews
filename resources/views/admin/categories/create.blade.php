@extends('layouts.app')

@section('content')

<div class='container'>
  <div class='row'>
    <div class='col-md-12'>
      <h3>New Category</h3>

      @include('common._form-errors')

      {!! Form::open(['route' => 'admin.categories.store', 'class' => 'form-horizontal']) !!}

      @include('admin.categories._fields', ['submitLabel' => 'Save Category'])

      {!! Form::close() !!}
    </div>
  </div>
</div>

@endsection