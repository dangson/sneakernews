@extends('layouts.app')

@section('content')

<div class='container'>
  <div class='row'>
    <div class='col-md-12'>
      <h3>Categories</h3>

      @include('common._flashed-message')

      <p>
        <a href="{{ route('admin.categories.create') }}" class='btn btn-default'>Add Category</a>
      </p>

      <table class='table table-striped table-bordered'>
        <thead>
          <tr>
            <th>Category</th>
            <th width='15%'>Order</th>
            <th width='15%'># of Feeds</th>
            <th width='15%'>Edit</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($categories AS $category)
            <tr>
              <td>{{ $category->name }}</td>
              <td>{{ $category->display_order }}</td>
              <td>{{ $category->feeds->count() }}</td>
              <td><a href="{{ route('admin.categories.edit', $category->id) }}">Edit</a></td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection