<div class='form-group'>
  <label for='name' class='col-md-2 control-label'><span class='required'>*</span> Name:</label>
  <div class='col-md-4'>
    {!! Form::text('name', $category->name, ['class' => 'form-control']) !!}
  </div>
</div>

<div class='form-group'>
  <label for='display_order' class='col-md-2 control-label'><span class='required'>*</span> Order:</label>
  <div class='col-md-1'>
    {!! Form::text('display_order', $category->display_order, ['class' => 'form-control']) !!}
  </div>
</div>

<div class='form-group'>
  <div class='col-md-10 col-md-offset-2'>
    {!! Form::submit($submitLabel, ['class' => 'btn btn-default']) !!}
  </div>
</div>