<?php $feedsDisplayed = 0; ?>

@foreach ($category->feeds AS $feed)
  <div class='col-md-4 col-sm-6'>
    <h3>{{ $feed->name }}</h3>

    @if (count($feed->latestPosts) > 0)
      <ul class='list-posts'>
        @foreach ($feed->latestPosts AS $post)
          <li>
            <a href='{{ $post->url }}' class='post-link' data-content="{{ $post->content_shortened }}">{{ $post->title }}</a>
          </li>
        @endforeach
      </ul>
    @endif
  </div>

  <?php $feedsDisplayed++; ?>

  @if ($feedsDisplayed % 3 === 0)
    <div class='clearfix visible-lg-block visible-md-block hidden-sm hidden-xs'>&nbsp;</div>
  @endif

  @if ($feedsDisplayed % 2 === 0)
    <div class='clearfix visible-sm-block hidden-lg hidden-md hidden-xs'>&nbsp;</div>
  @endif
@endforeach