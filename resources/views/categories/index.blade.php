@extends('layouts.default')

@section('content')

@foreach ($categories AS $category)
	<div class='row'>
		<div class='col-md-12'>
			<h2>{{ $category->name }}</h2>
		</div>

		@include('categories._display-feeds')
	</div>
@endforeach

@endsection