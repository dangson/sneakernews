<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(SneakerNews\User::class, function (Faker\Generator $faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->email,
        'password'       => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(SneakerNews\Category::class, function (Faker\Generator $fake) {
    return [
        'name'          => 'Test Category',
        'display_order' => rand(0, 99),
    ];
});

$factory->define(SneakerNews\Feed::class, function (Faker\Generator $fake) {
    return [
        'name'        => 'Test Feed',
        'url'         => 'http://feeds.bbci.co.uk/news/rss.xml',
        'category_id' => function() {
            return factory(SneakerNews\Category::class)->create()->id;
        }
    ];
});

$factory->define(SneakerNews\Post::class, function (Faker\Generator $fake) {
    return [
        'pico_id'      => str_random(10),
        'title'        => 'Test News Post',
        'url'          => 'http://feeds.bbci.co.uk/news/rss.xml',
        'publish_date' => '2015-01-01 12:00:00',
        'author'       => $fake->name,
        'content'      => 'Test post content',
        'feed_id'      => function() {
            return factory(SneakerNews\Feed::class)->create()->id;
        }
    ];
});