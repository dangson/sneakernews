#!/bin/bash

cd "$(dirname "$0")"
git pull
composer install
npm install
gulp --production
php artisan migrate
php artisan view:clear
php artisan route:clear